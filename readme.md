﻿### CVD slot finder
It requires Chrome v90.x to run

### How to run
`mkdir <folder-name>`  
`cd <folder-name>`  
`git clone <this-repo>`  
`cd cvdrdv/cvdrdv`  
`echo bla-bla > settings.txt`  
//edit settings.txt - see below how-to  
`dotnet build`  
`dotnet run`  

### Settings
Text file with 5 lines:  
0. string: DL login
1. string: DL password
2. string: DL search url - details see below
3. int: Result max number // if no need limit, put big number like 9000  

Also, settings could be provided as list of arguments, but it wasn't tested.  

### DL Search url
Make first search manually with configured location and age category.  
As you get page with list of clinics and slots, copy url to settings file.
