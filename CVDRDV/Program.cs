﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;

namespace CVDRDV
{
    class Program
    {
        static void Main(string[] args)
        {
            var settings = Settings.Load(args);

            using var driver = DriverHelper.BuildDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://www.doctolib.fr/sessions/new");
            driver.Auth(settings.Login, settings.Password);
            driver.GoToMain();
            driver.Navigate().GoToUrl(settings.SearchUrl);
            var urls = driver.GoOverSearchResult();
            for (var i = 0; i < urls.Count; i++)
            {
                driver.ExecuteScript("window.open();");
                driver.SwitchTo().Window(driver.WindowHandles[i + 1]);
                driver.Navigate().GoToUrl(urls[i]);
            }

            Console.WriteLine("Done. Press any key to close Chrome and exit...");
            Console.ReadKey();
        }
    }

    public static class PageExtensions
    {
        public static void Auth(this ChromeDriver driver, string login, string password)
        {
            driver.WaitDisplayed(By.Id("username"), 10.Seconds());
            driver.SetField("username", login);
            driver.FindElements(By.Id("password")).First(x => x.Displayed).SendKeys(password);
            driver.ClickTo(By.ClassName("dl-button-DEPRECATED_yellow"));
            
        }
        public static void GoToMain(this ChromeDriver driver)
        {
            var selector = By.ClassName("logo-doctolib");
            driver.WaitDisplayed(selector, 10.Seconds());
            driver.FindElement(selector).Click();
        }

        public static List<string> GoOverSearchResult(this ChromeDriver driver)
        {
            var urls = new List<string>();
            while (true)
            {
                var itemsSelector = By.ClassName("dl-search-result");
                driver.WaitDisplayed(itemsSelector, 10.Seconds());
                var items = driver.FindElements(itemsSelector);

                foreach (var item in items)
                {
                    driver.ExecuteScript("arguments[0].scrollIntoView(true);", item);
                    Thread.Sleep(100);

                    new WebDriverWait(driver, 10.Seconds())
                        .Until(wd => item.FindElement(By.ClassName("js-dl-search-results-calendar")).Displayed &&
                                     item.FindElement(By.ClassName("loaded")).Displayed);

                    var currentItemText = item.Text;
                    if (FailMessages.Any(msg => currentItemText.Contains(msg)))
                        continue;

                    urls.Add(item.FindElement(By.TagName("a")).GetAttribute("href"));
                }

                var nextButton = driver.FindElement(By.ClassName("next"));
                if (nextButton.FindElements(By.ClassName("disabled")).Count > 0)
                    return urls;

                nextButton.Click();
                driver.WaitPageLoaded();
            }
        }

        private static void WaitPageLoaded(this ChromeDriver driver)
        {
            new WebDriverWait(driver, 10.Seconds())
                .Until(wd => driver.ExecuteScript("return document.readyState").Equals("complete"));
        }

        private static readonly string[] FailMessages = {
            "Aucun rendez-vous",
            "Désolé, une erreur est survenue",
            "Prochain RDV"
        };
    }

    public static class DriverHelper
    {
        public static ChromeDriver BuildDriver()
        {
            return new ChromeDriver(GetDriverPath());
        }

        private static string GetDriverPath(int version = 90)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX)) return $"./Drivers/v{version}/";
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) return $"./Drivers/v{version}/";
            throw new Exception("Chrome driver is not configured for current OS");
        }
    }

    public static class DriverExtensions
    {
        public static void SetField(this RemoteWebDriver driver, string id, string value) => driver.FindElementById(id).SendKeys(value);

        public static void WaitDisplayed(this RemoteWebDriver driver, By selector, TimeSpan timeout) => new WebDriverWait(driver, timeout).Until(wd => wd.FindElement(selector).Displayed);

        public static void ClickTo(this RemoteWebDriver driver, By selector) => driver.FindElement(selector).Click();
    }

    public static class TimeSpanExtensions
    {
        public static TimeSpan Seconds(this int sec) => TimeSpan.FromSeconds(sec);
    }

    public class Settings
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string SearchUrl { get; set; }
        public int ResultMaxNumber { get; set; }

        private Settings() {}

        public static Settings Load(string[] lines)
        {
            if (File.Exists("settings.txt"))
            {
                lines = File.ReadAllLines("settings.txt");
            }

            if (lines.Length < 4)
                throw new Exception("Can't load settings");

            return new Settings
            {
                Login = lines[0],
                Password = lines[1],
                SearchUrl = lines[2],
                ResultMaxNumber = int.Parse(lines[3])
            };
        }
    }
}